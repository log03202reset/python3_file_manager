#!/usr/bin/env python3.11
import os
import sys
import curses
import shutil
import subprocess
import time
import signal
import psutil
#import fcntl


# Color pairs
NEW_COLOR = 1
BLUE_COLOR = 2
DIR_COLOR = 3
ARCHIVE_COLOR = 4
AUDIO_COLOR = 5
VIDEO_COLOR = 6
EXECUTABLE_COLOR = 7
TEXT_COLOR = 8


class FileManager:
    def __init__(self):
        self.original_cwd = os.getcwd()
        self.content = []
        self.directory = self.original_cwd
        self.selected_row = 0
        self.start_row = 0
        self.selected_items = set()
        self.selected_row_in_parent = 0


    def init_colors(self):
        curses.start_color()
        curses.init_pair(NEW_COLOR, curses.COLOR_BLACK, curses.COLOR_YELLOW)
        curses.init_pair(BLUE_COLOR, curses.COLOR_WHITE, curses.COLOR_BLUE)
        curses.init_pair(DIR_COLOR, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(ARCHIVE_COLOR, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(AUDIO_COLOR, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        curses.init_pair(VIDEO_COLOR, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        curses.init_pair(EXECUTABLE_COLOR, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(TEXT_COLOR, curses.COLOR_WHITE, curses.COLOR_BLACK)


    def format_file_size(self, size):
        suffixes = ['B', 'KiB', 'MiB', 'GiB']
        index = 0
        while size >= 1024 and index < len(suffixes) - 1:
            size /= 1024
            index += 1
        return f"{size:.2f} {suffixes[index]}"


    def rename_item(self, stdscr, selected_path):
        curses.echo()
        stdscr.addstr("Enter the new name: ")
        stdscr.refresh()
        new_name = None
        while True:
            key = stdscr.getch()
            if key == curses.KEY_ENTER or key == 10 or key == 13:
                break
            elif key == 27:
                new_name = None
                break
            else:
                try:
                    new_name = stdscr.getstr().decode('utf-8')
                except curses.error:
                    new_name = None
                    break
        curses.noecho()
        if new_name is not None:
            new_name = new_name.strip()
        return new_name


    def copy_items(self, stdscr):
        if not self.selected_items:
            stdscr.addstr("No item is selected to copy.\n")
            return

        for item in self.selected_items:
            selected_item = os.path.basename(item)
            destination_path = os.path.join(self.copy_directory, selected_item)
            try:
                if os.path.isfile(item):
                    shutil.copy(item, destination_path)
                elif os.path.isdir(item):
                    shutil.copytree(item, destination_path)
                stdscr.addstr(f"'{selected_item}' copied to {self.copy_directory}.\n")
            except Exception as error:
                stdscr.addstr(f"Error copying '{selected_item}': {str(error)}\n")

        self.selected_items.clear()
        stdscr.refresh()


    def move_items(self, stdscr, directory):
        if not self.selected_items:
            stdscr.addstr("No item is selected to move.\n")
            return

        total_items = len(self.selected_items)
        progress_bar_width = curses.COLS - 10
        num_sections = 10
        section_width = progress_bar_width // num_sections
        stdscr.addstr(f"Moving {total_items} item(s)...\n")
        stdscr.refresh()

        for i, item in enumerate(self.selected_items, start=1):
            try:
                destination_path = os.path.join(directory, os.path.basename(item))
                shutil.move(item, destination_path)
                progress = (i / total_items) * progress_bar_width
                num_filled_sections = int(progress / section_width)
                progress_bar = f"[{'=' * num_filled_sections}{' ' * (num_sections - num_filled_sections)}] {i}/{total_items}"
                stdscr.addstr(2, 0, progress_bar)
                stdscr.refresh()
                time.sleep(0.1)
            except Exception as error:
                stdscr.addstr(f"Error moving '{os.path.basename(item)}': {str(error)}\n")
                stdscr.refresh()

        stdscr.addstr("\nMove completed.\n")
        self.selected_items.clear()
        stdscr.refresh()


    def paste_items(self, stdscr, directory):
        if not self.selected_items:
            stdscr.addstr("No item is copied or moved to paste.\n")
        else:
            total_items = len(self.selected_items)
            progress_bar_width = curses.COLS - 10
            num_sections = 10
            section_width = progress_bar_width // num_sections
            stdscr.addstr(f"Pasting {total_items} item(s)...\n")
            stdscr.refresh()

            for i, item in enumerate(self.selected_items, start=1):
                try:
                    if self.operation == 'copy':
                        if os.path.isfile(item):
                            shutil.copy(item, directory)
                        elif os.path.isdir(item):
                            shutil.copytree(item, os.path.join(directory, os.path.basename(item)))
                    elif self.operation == 'move':
                        if os.path.isfile(item):
                            shutil.move(item, directory)
                        elif os.path.isdir(item):
                            shutil.move(item, os.path.join(directory, os.path.basename(item)))

                    progress = (i / total_items) * progress_bar_width
                    num_filled_sections = int(progress / section_width)
                    progress_bar = f"[{'=' * num_filled_sections}{' ' * (num_sections - num_filled_sections)}] {i}/{total_items}"
                    stdscr.addstr(2, 0, progress_bar)
                    stdscr.refresh()
                    time.sleep(0.1)
                except Exception as error:
                    stdscr.addstr(f"Error pasting '{os.path.basename(item)}': {str(error)}\n")
                    stdscr.refresh()

            stdscr.addstr("\nPaste completed.\n")

        self.selected_items.clear()
        self.copy_directory = ''
        self.operation = ''
        stdscr.refresh()


    def sigchld_handler(self, signum, frame):
        os.chdir(self.directory)


    def open_external_program(self, program, path):
#        original_cwd = os.getcwd()
        try:
            os.chdir(os.path.dirname(path))
            signal.signal(signal.SIGCHLD, self.sigchld_handler)
            if "DISPLAY" in os.environ:
                file_extension = os.path.splitext(path)[-1].lower()
#                video_extensions = {".mp3", ".m4a", ".webm", ".mp4", ".mkv", ".ts"}
                video_extensions = {".mp3", ".m4a"}
                if file_extension in video_extensions:
                    subprocess.Popen(f'"{program}" "{path}"', shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).wait()
                else:
                    subprocess.Popen([program, path]).wait()
            else:
                subprocess.Popen([program, path]).wait()
        except:
            self.display_directory_files(stdscr, directory)
#        finally:
#            os.chdir(original_cwd)


    def display_directory_files(self, stdscr, directory):
        try:
            content = os.listdir(directory)
        except PermissionError:
            stdscr.addstr("Permission denied")
            stdscr.refresh()
            return
        except FileNotFoundError:
            stdscr.addstr("Directory not found")
            stdscr.refresh()
            return
        except IndexError:
            stdscr.addstr("Directory not found")
            stdscr.refresh()
            return

        original_cwd = os.getcwd()

        stdscr.clear()
        initial_max_rows = curses.LINES - 8
        max_rows = initial_max_rows
        max_cols = stdscr.getmaxyx()[1]
        num_files = len(content)
        content.sort()
        start_row = 0
        selected_row = 0

        while True:
            stdscr.clear()

            if num_files == 0:
                stdscr.addstr(f"Content of {directory}:\n\n", curses.color_pair(NEW_COLOR))
                stdscr.addstr("Empty directory.")
                stdscr.refresh()
            else:
                stdscr.addstr(f"Content of {directory}:\n\n", curses.color_pair(NEW_COLOR))
                for i, item in enumerate(content[start_row:start_row + max_rows], start=start_row):
                    prefix = "> " if i == selected_row else "  "
                    file_path = os.path.join(directory, item)
                    if os.path.isdir(file_path):
                        stdscr.addstr(f"{prefix}[D] {item}\n", curses.color_pair(DIR_COLOR))
                    else:
                        try:
                            size = os.path.getsize(file_path)
                            formatted_size = self.format_file_size(size)
                            lowercase_item = item.lower()
                            lowercase_extensions = [extension.lower() for extension in (".tar", ".rar", ".zip", ".7z", ".bz2", ".lz4", ".xz")]

                            if any(lowercase_item.endswith(extension) for extension in lowercase_extensions):
                                try:
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n", curses.color_pair(ARCHIVE_COLOR))
                                except:
                                    self.display_directory_files(stdscr, directory)
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                            elif any(lowercase_item.endswith(extension) for extension in (".mp3", ".m4a", ".webm")):
                                try:
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n", curses.color_pair(AUDIO_COLOR))
                                except:
                                    self.display_directory_files(stdscr, directory)
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                            elif any(lowercase_item.endswith(extension) for extension in (".mp4", ".mkv", ".ts", ".jpeg", ".jpg", ".png", ".gif", ".mov", ".3gp", ".3g2", ".mj2")):
                                try:
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n", curses.color_pair(VIDEO_COLOR))
                                except:
                                    self.display_directory_files(stdscr, directory)
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                            elif os.access(file_path, os.X_OK):
                                try:
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n", curses.color_pair(EXECUTABLE_COLOR))
                                except:
                                    self.display_directory_files(stdscr, directory)
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                            else:
                                try:
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n", curses.color_pair(TEXT_COLOR))
                                except:
                                    self.display_directory_files(stdscr, directory)
                                    stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                        except FileNotFoundError:
                            try:
                                stdscr.addstr(f"{prefix}[F] {item} (File not found)\n", curses.color_pair(TEXT_COLOR))
                            except:
                                self.display_directory_files(stdscr, directory)
                                stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                        except IndexError:
                            try:
                                stdscr.addstr(f"{prefix}[F] {item} (File not found)\n", curses.color_pair(TEXT_COLOR))
                            except:
                                self.display_directory_files(stdscr, directory)
                                stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")
                        except PermissionError:
                            try:
                                stdscr.addstr(f"{prefix}[F] {item} (Permission denied)\n", curses.color_pair(TEXT_COLOR))
                            except:
                                self.display_directory_files(stdscr, directory)
                                stdscr.addstr(f"{prefix}[F] {item} ({formatted_size})\n")


            num_directories = 0
            num_files_only = 0
            for item in content:
                file_path = os.path.join(directory, item)
                if os.path.isdir(file_path):
                    num_directories += 1
                else:
                    num_files_only += 1
            try:
                stdscr.addstr(f"\nDirectories: {num_directories}, Files: {num_files_only} (Total: {num_files})\n\n")
            except:
                self.display_directory_files(stdscr, directory)

            stdscr.refresh()
            key = stdscr.getch()

            max_rows = curses.LINES - 8
            if key == curses.KEY_UP or key == ord('w'):
                stdscr.clear()
                stdscr.refresh()
                if selected_row > 0:
                    selected_row -= 1
                    if selected_row < start_row:
                        start_row = selected_row
                else:
                    num_files = len(content)
                    selected_row = num_files - 1
                    if num_files > max_rows:
                        start_row = selected_row - max_rows + 1
                    else:
                        start_row = 0

            elif key == curses.KEY_DOWN or key == ord('s'):
                stdscr.clear()
                stdscr.refresh()
                if selected_row < num_files - 1:
                    selected_row += 1
                    if selected_row >= start_row + max_rows:
                        start_row = selected_row - max_rows + 1
                else:
                    num_files = len(content)
                    selected_row = 0
                    start_row = 0

            elif key == curses.KEY_RIGHT or key == ord('d') or key == curses.KEY_ENTER or key == 10 or key == 13:
                parent_directory = os.path.dirname(directory)
                stdscr.clear()
                stdscr.refresh()
                try:
                    selected_item = content[selected_row]
                except IndexError:
                    if parent_directory:
                        try:
                            content = os.listdir(parent_directory)
                            directory = parent_directory
                            num_files = len(content)
                            content.sort()
                            start_row = 0
                            selected_row = self.selected_row_in_parent
                        except PermissionError:
                            stdscr.clear()
                            stdscr.addstr("Permission denied")
                            stdscr.refresh()
                        except FileNotFoundError:
                            stdscr.clear()
                            stdscr.addstr("Parent directory not found")
                            stdscr.refresh()
                try:
                    selected_path = os.path.join(directory, selected_item)

                    if os.path.isdir(selected_path):
                        self.selected_row_in_parent = selected_row
                        directory = selected_path
                        try:
                            content = os.listdir(directory)
                        except PermissionError:
                            stdscr.clear()
                            stdscr.addstr("Permission denied")
                            stdscr.refresh()
                        num_files = len(content)
                        content.sort()
                        start_row = 0
                        selected_row = 0
                    else:
                        curses.endwin()
                        program = input("Enter the program to open the file: ")
                        if program.strip():
                            try:
                                self.open_external_program(program, selected_path)
                            except:
                                stdscr.clear()
                                stdscr.refresh()
                                selected_path = os.path.join(directory, selected_item)
                        else:
                            print("No program provided. Ignoring ...")
                except:
                    self.display_directory_files(stdscr, directory)
                finally:
                    original_cwd = os.getcwd()
                    os.chdir(original_cwd)

            elif key == curses.KEY_LEFT or key == ord('a'):
                parent_directory = os.path.dirname(directory)
                if parent_directory:
                    try:
                        content = os.listdir(parent_directory)
                        directory = parent_directory
                        num_files = len(content)
                        content.sort()
                        start_row = 0
                        selected_row = self.selected_row_in_parent
                    except PermissionError:
                        stdscr.clear()
                        stdscr.addstr("Permission denied")
                        stdscr.refresh()
                    except FileNotFoundError:
                        stdscr.clear()
                        stdscr.addstr("Parent directory not found")
                        stdscr.refresh()
                else:
                    self.display_directory_files(stdscr, directory)

            elif key == ord('r'):
                selected_item = content[selected_row]
                selected_path = os.path.join(directory, selected_item)
                if os.path.exists(selected_path):
                    confirmation_msg = f"You're sure you want to remove '{selected_item}'? (y/n): "
                    stdscr.addstr(confirmation_msg)
                    stdscr.refresh()
                    curses.echo()
                    response = stdscr.getstr().decode('utf-8')
                    curses.noecho()
                    if response.lower() == 'y':
                        try:
                            if os.path.isfile(selected_path):
                                os.remove(selected_path)
                            elif os.path.isdir(selected_path):
                                shutil.rmtree(selected_path)
#                            stdscr.addstr(f"Removed: '{selected_item}'\n")
                            stdscr.refresh()
#                        except Exception as error:
#                            stdscr.addstr(f"Error Removing '{selected_item}': {str(error)}\n")
                        except:
                            stdscr.addstr("Error Removing selected_item\n")
                            stdscr.refresh()
                        finally:
                            original_cwd = os.getcwd()
                            os.chdir(original_cwd)
                    else:
                        stdscr.addstr(f"Canceled Removal Operation.\n")
                        stdscr.refresh()
                else:
                    self.display_directory_files(stdscr, directory)

            elif key == ord('R'):
                self.display_directory_files(stdscr, directory)

            elif key == ord('e') and stdscr.getch() == ord('e'):
                selected_item = content[selected_row]
                selected_path = os.path.join(directory, selected_item)
                if os.path.exists(selected_path):
                    curses.endwin()
                    new_name = self.rename_item(stdscr, selected_path)
                    if new_name is None:
                        continue
                    if new_name:
                        new_path = os.path.join(directory, new_name)
                        try:
                            os.rename(selected_path, new_path)
                            self.display_directory_files(stdscr, directory)
                        except Exception as error:
                            stdscr.addstr(f"Error renaming '{selected_item}': {str(error)}\n")
                            stdscr.refresh()
                    else:
                        stdscr.addstr("No changes made. Ignoring renaming.\n")
                        stdscr.refresh()

            elif key == ord('i'):
                total, used, free = shutil.disk_usage(directory)
                total_formatted = self.format_file_size(total)
                used_formatted = self.format_file_size(used)
                free_formatted = self.format_file_size(free)
                percent_used = (used / total) * 100
                percent_free = (free / total) * 100
                stdscr.addstr(f"Disk info: Used: {used_formatted} ({percent_used:.2f}%), Free: {free_formatted} ({percent_free:.2f}%), Total: {total_formatted}\n\n", curses.color_pair(NEW_COLOR))
                stdscr.getch()

            elif key == ord('v'):
                selected_item = content[selected_row]
                selected_path = os.path.join(directory, selected_item)
                stdscr.clear()
                try:
                    if os.path.isdir(selected_path):
                        total_size = 0
                        total_files = 0
                        total_directories = 0
                        for root, dirs, files in os.walk(selected_path):
                            for f in files:
                                file_path = os.path.join(root, f)
                                try:
                                    total_size += os.path.getsize(file_path)
                                    total_files += 1
                                except (PermissionError, FileNotFoundError, IndexError):
                                    continue
                            total_directories += len(dirs)
                        total_size_formatted = self.format_file_size(total_size)
                        soma_t = total_directories + total_files
                        stdscr.addstr(f"Directory Information for: '{selected_item}'\n\n")
                        stdscr.addstr(f"Total Size: {total_size_formatted}\n")
                        stdscr.addstr(f"Total Files: {total_files}\n")
                        stdscr.addstr(f"Total Directories: {total_directories}\n")
                        stdscr.addstr(f"Total: {soma_t}\n")
                    else:
                        file_size = os.path.getsize(selected_path)
                        file_size_formatted = self.format_file_size(file_size)
                        stdscr.addstr(f"'{selected_item}' is not a directory.\n\n")
                        stdscr.addstr(f"File Size: {file_size_formatted}\n")
                except (PermissionError, FileNotFoundError):
                    stdscr.addstr("Due to an error or permission denied, some directories and files have been omitted.\n")
                stdscr.refresh()
                stdscr.getch()

            elif key == ord('V'):
                stdscr.clear()
                stdscr.addstr(f"Directory information:", curses.color_pair(BLUE_COLOR))
                stdscr.addstr(f"\nCurrent directory: {directory}")
                stdscr.addstr(f"\nDirectories: {num_directories}")
                stdscr.addstr(f"\nFiles: {num_files_only}")
                stdscr.addstr(f"\nTotal: {num_files}\n\n")
                total, used, free = shutil.disk_usage(directory)
                total_formatted = self.format_file_size(total)
                used_formatted = self.format_file_size(used)
                free_formatted = self.format_file_size(free)
                percent_used = (used / total) * 100
                percent_free = (free / total) * 100
                stdscr.addstr(f"Disk information:", curses.color_pair(BLUE_COLOR))
                stdscr.addstr(f"\nUsed: {used_formatted} ({percent_used:.2f}%)\n")
                stdscr.addstr(f"Free: {free_formatted} ({percent_free:.2f}%)\n")
                stdscr.addstr(f"Total: {total_formatted}\n")
                stdscr.refresh()
                stdscr.getch()

            elif key == ord('y') and stdscr.getch() == ord('y'):
                selected_item = content[selected_row]
                selected_path = os.path.join(directory, selected_item)
                if os.path.exists(selected_path):
                    self.selected_items.add(selected_path)
                    stdscr.addstr(f"'{selected_item}' copied.\n")
                    self.copy_directory = directory
                    self.operation = 'copy'
                else:
                    stdscr.addstr(f"File or directory '{selected_item}' not found.\n")

            elif key == ord('u') and stdscr.getch() == ord('u'):
                selected_item = content[selected_row]
                selected_path = os.path.join(directory, selected_item)
                if os.path.exists(selected_path):
                    self.selected_items.add(selected_path)
                    stdscr.addstr(f"'{selected_item}' moved.\n")
                    self.copy_directory = directory
                    self.operation = 'move'
                else:
                    stdscr.addstr(f"File or directory '{selected_item}' not found.\n")

            elif key == ord('p') and stdscr.getch() == ord('p'):
                self.paste_items(stdscr, directory)

            elif key == ord('l') and stdscr.getch() == ord('e')  and stdscr.getch() == ord('s')  and stdscr.getch() == ord('s'):
                if shutil.which('less'):
                    selected_item = content[selected_row]
                    selected_path = os.path.join(directory, selected_item)
                    if os.path.isfile(selected_path):
                        try:
                            curses.endwin()
                            os.system(f"less {selected_path}")
                            curses.initscr()
                            stdscr.clear()
                            stdscr.refresh()
                        except Exception as error:
                            curses.initscr()
                            stdscr.addstr(f"Error reading '{selected_item}': {str(error)}\n")
                            stdscr.refresh()
                else:
                    stdscr.addstr("Less is not installed or not in the system's PATH.\n")
                stdscr.refresh()
                time.sleep(0.5)

            elif key == ord('m') and stdscr.getch() == ord('o') and stdscr.getch() == ord('r') and stdscr.getch() == ord('e'):
                if shutil.which('more'):
                    selected_item = content[selected_row]
                    selected_path = os.path.join(directory, selected_item)
                    if os.path.isfile(selected_path):
                        try:
                            curses.endwin()
                            os.system(f"more {selected_path}")
                            curses.initscr()
                            stdscr.clear()
                            stdscr.refresh()
                        except Exception as error:
                            curses.initscr()
                            stdscr.addstr(f"Error reading '{selected_item}': {str(error)}\n")
                            stdscr.refresh()
                else:
                    stdscr.addstr("More is not installed or not in the system's PATH.\n")
                stdscr.refresh()
                time.sleep(0.5)

            elif key == ord('b') and stdscr.getch() == ord('a') and stdscr.getch() == ord('t'):
                if shutil.which('bat'):
                    selected_item = content[selected_row]
                    selected_path = os.path.join(directory, selected_item)
                    if os.path.isfile(selected_path):
                        try:
                            curses.endwin()
                            os.system(f"bat {selected_path}")
                            curses.initscr()
                            stdscr.clear()
                            stdscr.refresh()
                        except Exception as error:
                            curses.initscr()
                            stdscr.addstr(f"Error reading '{selected_item}': {str(error)}\n")
                            stdscr.refresh()
                else:
                    stdscr.addstr("Bat is not installed or not in the system's PATH.\n")
                stdscr.refresh()
                time.sleep(0.5)

            elif key == ord('T'):
                if "DISPLAY" in os.environ:
                    if shutil.which('alacritty'):
                        try:
                            subprocess.Popen(['alacritty'])
                        except Exception as error:
                            stdscr.addstr(f"Error opening terminal: {error}")
                    else:
                        stdscr.addstr("Alacritty is not installed or not in the system's PATH.\n")
                    stdscr.refresh()
                    time.sleep(0.5)
                else:
                    pass

            elif key == ord('o'):
                content.sort()
                selected_row = 0
                start_row = 0

            elif key == ord('O'):
                content.sort(reverse=True)
                selected_row = 0
                start_row = 0

            elif key == ord('m'):
                content.sort(key=lambda item: os.path.getsize(os.path.join(directory, item)))
                selected_row = 0
                start_row = 0

            elif key == ord('M'):
                content.sort(key=lambda item: os.path.getsize(os.path.join(directory, item)), reverse=True)
                selected_row = 0
                start_row = 0

            elif key == ord('/'):
                stdscr.addstr("Enter the search term: ")
                stdscr.refresh()
                curses.echo()
                search_term = stdscr.getstr().decode('utf-8')
                curses.noecho()
                matching_files = []
                matching_directories = []
                for item in content:
                    file_path = os.path.join(directory, item)
                    if search_term.lower() in item.lower():
                        if os.path.isfile(file_path):
                            matching_files.append(item)
                        elif os.path.isdir(file_path):
                            matching_directories.append(item)
                if matching_files or matching_directories:
                    stdscr.clear()
                    matching_items = matching_directories + matching_files
                    count_items_match = len(matching_items)
                    stdscr.addstr(f"{count_items_match} files and/or directories found for the term '{search_term}'\n\n")
                    max_results = curses.LINES - 4
                    start_index = 0
                    end_index = min(len(matching_items), max_results)
                    page_items = matching_items[start_index:end_index]
                    for i, item in enumerate(page_items, start=start_index + 1):
                        stdscr.addstr(f"{i}. {item}\n")
                    stdscr.refresh()
                    if key == ord(' ') and end_index < count_items_match:
                        start_index += max_results
                        end_index += max_results
                        if end_index > count_items_match:
                            end_index = count_items_match
                        page_items = matching_items[start_index:end_index]
                        for i, item in enumerate(page_items, start=start_index + 1):
                            stdscr.addstr(f"{i}. {item}\n")
                        stdscr.refresh()
                    key = stdscr.getch()
                else:
                    stdscr.clear()
                    stdscr.addstr(f"No files or directories found for the term '{search_term}'")
                stdscr.refresh()
                stdscr.getch()

            elif key == ord('h'):
                stdscr.clear()
                stdscr.addstr("Options:\n\n")
                stdscr.addstr("  w               Up\n")
                stdscr.addstr("  s               Down\n")
                stdscr.addstr("  d               Right\n")
                stdscr.addstr("  a               Left\n")
                stdscr.addstr("  r               Remove\n")
                stdscr.addstr("  R               Reload\n")
                stdscr.addstr("  e + e           Rename\n")
                stdscr.addstr("  i               Disk Info\n")
                stdscr.addstr("  v               Selected directory information\n")
                stdscr.addstr("  V               Current directory information\n")
                stdscr.addstr("  y + y           Copy\n")
                stdscr.addstr("  u + u           Move\n")
                stdscr.addstr("  p + p           Paste\n")
                stdscr.addstr("  l + e + s + s   View content (less)\n")
                stdscr.addstr("  m + o + r + e   View content (more)\n")
                stdscr.addstr("  b + a + t       View content (bat)\n")
                stdscr.addstr("  T               Open terminal\n")
                stdscr.addstr("  o               Alphabetical order\n")
                stdscr.addstr("  O               Reverse alphabetical order\n")
                stdscr.addstr("  m               From smallest to largest\n")
                stdscr.addstr("  M               From largest to smallest\n")
                stdscr.addstr("  /               Search\n")
                stdscr.addstr("  q               Quit\n")
                stdscr.refresh()
                stdscr.getch()

            elif key == ord('q'):
                stdscr.clear()
                stdscr.refresh()
                sys.exit(0)

            if selected_row - start_row >= max_rows:
                start_row = selected_row - max_rows + 1
            elif selected_row < start_row:
                start_row = selected_row

        os.chdir(original_cwd)


    def main(self, stdscr):
        curses.curs_set(0)
        self.init_colors()
        script_directory = os.path.dirname(os.path.abspath(__file__))
        current_directory = os.getcwd()
        original_cwd = os.getcwd()
        stdscr.clear()
        stdscr.refresh()
        try:
            self.display_directory_files(stdscr, current_directory)
        except:
            stdscr.clear()
            stdscr.refresh()
            self.display_directory_files(stdscr, current_directory)


if __name__ == "__main__":
    try:
        file_manager = FileManager()
        curses.wrapper(file_manager.main)
    except KeyboardInterrupt:
        sys.exit()
